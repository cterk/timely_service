<?php
/**
 * Created by PhpStorm.
 * Author: baihu
 * Date: 2019/12/16
 * Time: 17:55
 */

namespace app\admin\controller;

use Logic\KefuLogic;
use think\Validate;
class Kefu extends  Base
{

    /**
     * 客服列表
     * @param $param
     * @return array
     */
    public  function  index(){
        if (request()->isAjax()) {
            $limit = input('param.limit');
            $where = [];
            if (input('param.name')) {
                $where[] = ['kefu_name', '=', input('param.name')];
            }
            if ( input('param.online') == 1 || input('param.online') ==2) {
                $where[] = ['online_status', '=', input('param.online')];
            }
            $list = KefuLogic::getKefuByPage($where, '*', $limit);
            return $this->response->api($list->items() ? $list->items() : '',self::SUCCESS_CODE,'操作成功',['count'=>$list->total()]);
        }
        return $this->fetch();
    }

    /**
     * 添加客服
     * @param $param
     * @return array
     */
    public function addKefu()
    {
        $param = input('param.');
        $validate = Validate::make([
            'kefu_name' => 'require|max:50',
            'kefu_password' => 'require'
        ], [
            'kefu_name.require' => '客服名必须',
            'kefu_name.max' => '客服名最多不能超过50个字符',
            'kefu_password.require' => '客服密码必须',

        ]);
        if (!$validate->check($param)) {
            return $this->response->api('', 1001, $validate->getError());
        }
        KefuLogic::addKefu($param['kefu_name'], $param['kefu_password']);
        return $this->response->api('', self::SUCCESS_CODE, '添加成功');
    }
    /**
     * 编辑客服
     * @param $param
     * @return array
     */
    public function updateKefu()
    {
        $param = input('post.');
        $validate = Validate::make([
            'kefu_name' => 'require|max:50',
            'kefu_id' => 'require',

        ], [
            'kf_name.require' => '客服名必须',
            'kf_name.max' => '客服名最多不能超过50个字符',
            'kefu_id.require' => '记录id必须'
        ]);
        if (!$validate->check($param)) {
            return $this->response->api('', 1001, $validate->getError());
        }
        KefuLogic::saveKefu($param['kefu_id'], $param['kefu_name']);

        return $this->response->api('', self::SUCCESS_CODE, '修改成功');
    }
    /**
     * 修改客服密码
     * @param $param
     * @return array
     */
    public  function  resetPassword(){
        if( session('admin_id')!= 1){
            return $this->response->api('', 1001, '暂无权限');
        }
        $param = input('post.');
        $validate = Validate::make([
            'password' => 'require|max:50',
            'kefu_id' => 'require',

        ], [
            'kf_name.require' => '密码必须',
            'kf_name.max' => '密码最多不能超过50个字符',
            'kefu_id.require' => '记录id必须'
        ]);
        if (!$validate->check($param)) {
            return $this->response->api('', 1001, $validate->getError());
        }
        KefuLogic::resetPassword($param['kefu_id'], $param['password']);
        return $this->response->api('',self::SUCCESS_CODE,'修改密码成功');
    }
    /**
     * 删除客服
     * @param $param
     * @return array
     */
    public function delKefu()
    {
        if( session('admin_id')!= 1){
            return $this->response->api('', 1001, '暂无权限');
        }
        $id = input('post.kid/d');
        KefuLogic::delKefu($id);
        return $this->response->api('', self::SUCCESS_CODE, '删除成功');
    }

}
