<?php
/**
 * Created by PhpStorm.
 * Author: baihu
 * Date: 2019/12/16
 * Time: 22:13
 */

namespace app\admin\controller;

use Logic\Visitor as VisitorLogic;
use Logic\ChatLogLogic;
class Visitor extends  Base
{
    /**
     * 游客列表
     * @param $param
     * @return array
     */
    public function  visitorList(){

        if (request()->isAjax()) {
            $limit = input('param.limit');
            $list = VisitorLogic::getVisitorList( $limit);
            return $this->response->api($list->items() ? $list->items() : '', self::SUCCESS_CODE, '操作成功', ['count' => $list->total()]);
        }
        return $this->fetch();
    }

    /**
     * 获取聊天记录
     * @param $param
     * @return array
     */
    public  function  chatLogList(){
        if (request()->isAjax()) {
        $name =input('param.name');
        $limit = input('param.limit');
        $list = ChatLogLogic::getChatLogList('',$name, $limit);
        return $this->response->api($list->items() ? $list->items() : '', self::SUCCESS_CODE, '操作成功', ['count' => $list->total()]);
        }
        return $this->fetch();

    }
    /**
     * 获取服务记录列表
     * @param $param
     * @return array
     */
    public  function  serviceList(){
        if (request()->isAjax()) {
            $limit = input('param.limit');
            $name =input('param.name');
            $where=[];
            if($name){
                $where[]=['kefu_name','=',$name];
            }
            $list = VisitorLogic::getServiceList($where, $limit);
            return $this->response->api($list->items() ? $list->items() : '', self::SUCCESS_CODE, '操作成功', ['count' => $list->total()]);
        }
        return $this->fetch();
    }
}