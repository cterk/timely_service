<?php
/**
 * Created by PhpStorm.
 * User: zhc
 * Date: 2019/10/22
 * Time: 11:07
 */

namespace Logic;

use think\Db;
class VisitorService
{

    /**
     * 获取今日会话次数
     * @return array
     */
    public  static  function getServiceNum($date=''){
        if(!$date){
            $date=date('Y-m-d');
        }
        $date=date('Y-m-d',strtotime($date));
        $endDate=date('Y-m-d 23:59:59',strtotime($date));
        return Db::name('visitor_service_log')->where('start_date','between time',[$date, $endDate])->count();
    }


    public  static  function  addServiceLog($data){
        return  Db::name('visitor_service_log')->insertGetId($data);
    }
    public  static  function  setEndTimeEndId($visitor_id){
        //获取最新的记录
        $info= Db::name('visitor_service_log')->where('visitor_id',$visitor_id)->order('start_date desc')->find();
        if($info){
            return  Db::name('visitor_service_log')->where('vsid',$info['vsid'])->update([
                'end_date'=>date('Y-m-d H:i:s'),
                'connect_stauts'=>2,
            ]);
        }else{
            return  true;
        }

    }


}
